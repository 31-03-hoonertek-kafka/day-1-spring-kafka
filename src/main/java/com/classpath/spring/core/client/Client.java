package com.classpath.spring.core.client;

import com.classpath.spring.core.passenger.Passenger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        Passenger passenger = applicationContext.getBean("passenger", Passenger.class);
        passenger.travel("BTM-Layout", "Bangalore-International-airport");
    }
}
