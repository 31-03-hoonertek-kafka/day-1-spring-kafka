package com.classpath.spring.core.passenger;

import com.classpath.spring.core.commute.Driver;
import org.springframework.stereotype.Component;

@Component
public class Passenger {

    private final Driver driver;

    public Passenger(Driver driver) {
        this.driver = driver;
    }


    public void travel(String from, String destination){
        this.driver.commute(from, destination);
    }
}
