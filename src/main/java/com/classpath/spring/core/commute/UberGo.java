package com.classpath.spring.core.commute;

import org.springframework.stereotype.Component;

@Component
public class UberGo implements Driver {

    public void commute(String from, String destination){
        System.out.println("Commuting from :: "+ from + " to "+ destination + " with Uber-Go !!");
    }
}
