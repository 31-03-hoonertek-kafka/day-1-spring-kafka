package com.classpath.spring.core.commute;

public interface Driver {

    void commute(String from, String destination);
}
